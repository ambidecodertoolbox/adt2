# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 17:36:42 2015

@author: heller
"""

from __future__ import division, print_function

import symbolic_spherical_harmonics as ssh

import faustcode as fc


def panner_gain_expressions(channels, **kwargs):
    fc.print_faust_vector("pg",
                          (fc.faust_code(ssh.Ylm(ch, **kwargs))
                           for ch in channels))


def unit_tests():
    pass

if __name__ == '__main__':
    unit_tests()
