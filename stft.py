# -*- coding: utf-8 -*-
"""
Created on Mon May 25 12:49:31 2015

@author: heller
"""

from __future__ import division, print_function

import numpy as np
import scipy as sp

# for unit tests
from scikits import audiolab as al
from contextlib import closing
import matplotlib as mpl
from matplotlib import pyplot as plt
import pylab


def stft(x, fftsize=512, overlap=4):
    hop = int(fftsize / overlap)
    w = sp.hanning(fftsize+1)[:-1]      # better reconstruction with this trick +1)[:-1]
    return np.array([np.fft.rfft(w*x[i:i+fftsize])
                    for i in range(0, len(x)-fftsize, hop)])


def istft(X, overlap=4):
    fftsize = (X.shape[1]-1) * 2
    hop = int(fftsize / overlap)
    w = sp.hanning(fftsize+1)[:-1]
    x = np.zeros(X.shape[0]*hop)
    wsum = sp.zeros(X.shape[0]*hop)
    for n, i in enumerate(range(0, len(x)-fftsize, hop)):
        x[i:i+fftsize] += sp.real(np.fft.irfft(X[n])) * w   # overlap-add
        wsum[i:i+fftsize] += w ** 2.
    pos = wsum != 0
    x[pos] /= wsum[pos]
    return x


# unit tests
#  Plot the magnitude spectrogram.
def plot_magnitude_spectrogram(X):
    pylab.figure()
    pylab.imshow(X.T, origin='lower', aspect='auto',
                 interpolation='nearest')
    pylab.xlabel('Time')
    pylab.ylabel('Frequency')
    pylab.colorbar()
    pylab.show()


def read_wavfile(filename):
    with closing(al.Sndfile(filename, 'r')) as s:
        Fs, nc, enc, fmt = (s.samplerate, s.channels, s.encoding, s.format)
        print(Fs, nc, enc, fmt)
        B = s.read_frames(s.nframes, dtype=np.float64)
        print(np.shape(B))
        return Fs, B

def energy(X):
    return np.real(X * np.conj(X))


def unit_test(n=0):
    filename = '/Users/heller/masters/AJH_eight-positions.amb'
    filename = '/Users/heller/masters/Dvorak_CarnivalOverture_Op92-WXYZ.amb'
    Fs, B = read_wavfile(filename)
    P, X, Y, Z = np.sqrt(2) * B[:, 0], B[:, 1], B[:, 2], B[:, 3]

    fp, fx, fy, fz = stft(P), stft(X), stft(Y), stft(Z)
    Ix = np.real(fp * fx)
    Iy = np.real(fp * fy)
    Iz = np.real(fp * fz)

    E = (energy(fp) + energy(fx) + energy(fy) + energy(fz))/2

    D = 1 - np.sqrt(Ix**2 + Iy**2 + Iz**2) / (E + np.spacing(1))

    plot_magnitude_spectrogram(10*np.log10(E))
    plot_magnitude_spectrogram(D)

    WW = np.sqrt(0.5) * istft(fp)
    XX = istft(fx)
    YY = istft(fy)
    ZZ = istft(fz)

    al.wavwrite(WW, '/tmp/PP.wav', fs=Fs)


if __name__ == '__main__':

    unit_test(0)
