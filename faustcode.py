# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 18:56:32 2015

@author: heller
"""

from __future__ import division, print_function

from sympy.printing.ccode import CCodePrinter


class FaustCodePrinter (CCodePrinter):

    def _print_Rational(self, expr):
        p, q = int(expr.p), int(expr.q)
        return '%d/%d' % (p, q)

    def _print_Pi(self, expr):
        return 'math.PI'

    def _print_Matrix(self, expr):
        return 'MATRIX!'

    def _print_ImmutableMatrix(self, expr):
        return "("+", ".join((faust_code(e) for e in expr))+")"


def faust_code(expr, assign_to=None, **settings):
    return FaustCodePrinter().doprint(expr)


def print_faust_code(expr, **settings):
    print(faust_code(expr, **settings))


def print_faust_vector(assign_to, v, line_end=",\n", **kwargs):
    print(assign_to, " = (",
          ",\n\t".join(map(str, v)),
          ");")



def unit_tests():
    pass

if __name__ == '__main__':
    unit_tests()

