# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 16:11:45 2015

@author: heller
"""
from __future__ import division, print_function

#  from scipy.io import wavfile
from os import path, listdir, makedirs

import numpy as np

from scikits import audiolab as al
from scikits import samplerate as src


kernel_dir = '/Users/heller/data/kernels'
hrir_names = ('HRIR_W', 'HRIR_X', 'HRIR_Y', 'HRIR_Z')
subject_nums = listdir(path.join(kernel_dir, 'FOA/decoders/cipic/44100/512'))

#hrir_path = path.join(kernel_dir, 'FOA/decoders/cipic/44100/512',
#                         subject_nums[subject], hrir_names[component]+'.wav')


def read_kernel(hrir_path, target_Fs=48000):
    (y, Fs, enc) = al.wavread(hrir_path)
    print(hrir_path, Fs, enc, np.shape(y))
    if target_Fs is None or target_Fs != Fs:
        y48 = src.resample(y, target_Fs/Fs, 'sinc_best')
    else:
        y48 = y
        target_Fs = Fs
    return y48, target_Fs


def convert_all_hrirs():
    for subject in range(1, len(subject_nums)):
        for component in range(4):
            (y, Fs) = read_kernel(subject, component)
            out_dir = path.join(kernel_dir,
                                ('FOA/decoders/cipic/%d/%d/%s' %
                                 (Fs, np.shape(y)[0], subject_nums[subject])))

            try:
                makedirs(out_dir)
            except:
                pass

            al.wavwrite(y[:, 0],
                        path.join(out_dir, hrir_names[component] + '-left.wav'),
                        fs=Fs, enc='float32')
            al.wavwrite(y[:, 1],
                        path.join(out_dir, hrir_names[component] + '-right.wav'),
                        fs=Fs, enc='float32')


# scikits.audiolab.available_encodings('wav') to find possible encodings
def write_wav(f, y, Fs):
    al.wavwrite(y, f, fs=Fs, enc='float32')


def write_faust_array(f, y, var_name, fmt='%.18e'):
    f.write("%s = (\n" % var_name)
    np.savetxt(f, y[0:-1], delimiter=',', newline=',\n', fmt=fmt)
    f.write((fmt+'\n);\n') % y[-1])


def write_faust_kernels(subject):
    with open('kernels_%04d.dsp' % subject, mode='w') as f:
        (y, Fs) = read_kernel(subject, 0)
        write_faust_array(f, y[:, 0], "Hwl(%04d)" % subject)
        write_faust_array(f, y[:, 1], "Hwr(%04d)" % subject)

        (y, Fs) = read_kernel(subject, 1)
        write_faust_array(f, y[:, 0], "Hxl(%04d)" % subject)
        write_faust_array(f, y[:, 1], "Hxr(%04d)" % subject)

        (y, Fs) = read_kernel(subject, 2)
        write_faust_array(f, y[:, 0], "Hyl(%04d)" % subject)
        write_faust_array(f, y[:, 1], "Hyr(%04d)" % subject)

        (y, Fs) = read_kernel(subject, 3)
        write_faust_array(f, y[:, 0], "Hzl(%04d)" % subject)
        write_faust_array(f, y[:, 1], "Hzr(%04d)" % subject)
