# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 18:08:51 2015

@author: heller
"""

# python 3 compatbility
from __future__ import print_function
from __future__ import division

import numpy.polynomial.legendre as P
import numpy.polynomial.chebyshev as T


def max_rE_3d(order):
    x = max(P.Legendre.basis(order+1).roots())
    return [P.Legendre.basis(o)(x) for o in range(order+1)]


def max_rE_2d(order):
    x = max(T.Chebyshev.basis(order+1).roots())
    return [T.Chebyshev.basis(o)(x) for o in range(order+1)]

print(max_rE_2d(4))

print(max_rE_3d(4))
