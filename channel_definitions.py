# -*- coding: utf-8 -*-
"""
Created on Sat Apr  4 11:14:00 2015

@author: heller
"""
# python 3.0 compatibility
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy as sp

import enum as en


class ChannelOrderRules(en.Enum):
    ACN = 0
    ambiX = 0
    FuMa = 1
    AMB = FuMa
    SID = 2
    MPEG = 2


class NormalizationRules(en.Enum):
    N3D = 0
    SN3D = 1
    N2D = 2
    SN2D = 3
    MAX = 4
    FuMa = 5
    AMB = FuMa


class Conventions(en.Enum):
    ambiX2009 = NormalizationRules.N3D
    ambiX = NormalizationRules.SN3D
    AMB = NormalizationRules.AMB
    MPEG = ChannelOrderRules.MPEG


class ChannelDefinitions(object):

    def __init__(self, h_order, v_order=None, convention=None,
                 channel_order=None, normalization=None):

        if v_order is None:
            v_order = h_order
        if max(h_order, v_order) <= 3:
            convention = AMB
        else:
            convention = ambiX

        self.h_order = h_order
        self.v_order = v_order

    def set_channel_order(self, channel_order=None, convention=None):
        if channel_order:
            self.channel_order = channel_order
        elif convention:
            pass
