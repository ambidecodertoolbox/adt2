# Ambisonic Decoder Toolbox 2 (ADT2) #

This is a sandbox to hold my experiments for a Python version of the ADT.

The most useful functions currently are the SymPy code to 

* Derive symbolic expressions for the spherical harmonics in both cartesian and spherical coordinates.
* Confirm they are orthonormal.
* Determine the maximum value of each, which is used for MaxN and FuMa normalization.  If possible this is done symbolically, if that fails, the code falls back to a numeric solution. 
* Code to printout a subset of SymPy expressions in Faust syntax, which is useful for making Ambisonic panners.

Symbolic Spherical Harmonics command line

```

adt2$ python symbolic_spherical_harmonics.py --help
usage: symbolic_spherical_harmonics.py [-h] [-v] [-s] [--four_pi | --unity]
                                       [--orthonormal | --seminormalized | --unnormalized | --maxN | --fumaN]
                                       [-a | -f] [-c]
                                       [-t {c,mathml,javascript,none,fortran,faust,pretty}]
                                       degree [order]

returns experession for the real spherical harmonic of the specified order (l)
and degree (m)

positional arguments:
  degree                l, degree of the spherical harmonic. l>=0
  order                 m, order of the spherical harmonic. |m|<=l

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -s, --spherical       use spherical coordinates (default is Cartesian)
  --four_pi             inner product is 4pi
  --unity               inner product is 1
  --orthonormal         fully orthonormalized
  --seminormalized      Schmidt seminormalized
  --unnormalized        no normalization
  --maxN                max gain is 1 (note: may take a while to compute and
                        result may not be exact for l>3
  --fumaN               Furse-Malham normalization (see note on maxN)
  -a, --acn             interpret degree as ACN-index, m is ignored
  -f, --fuma            interpret degree as FuMa-index, m is ignored
  -c, --condon_shortley_phase
  -t {c,mathml,javascript,none,fortran,faust,pretty}, --translation {c,mathml,javascript,none,fortran,faust,pretty}

```


