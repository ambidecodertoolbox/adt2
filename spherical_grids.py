# -*- coding: utf-8 -*-
"""
Created on Sun May  3 17:56:47 2015

@author: heller
"""

from __future__ import division, print_function

import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def sph2cart(theta, phi, rho=1.0):
    return (np.cos(theta) * np.cos(phi) * rho,
            np.sin(theta) * np.cos(phi) * rho,
            np.sin(phi) * rho)


def equal_angle(n=10, m=None):
    if m is None:
        m = 2 * n
    q = np.arange(2 * n + 2) + 1/2
    l = np.arange(2 * m + 2)

    theta = q * np.pi / (2*n + 2)
    phi = l * 2*np.pi / (2*m + 2)

    theta -= np.pi/2
    phi -= np.pi

    azimuth, elevation = np.meshgrid(phi, theta)
    x, y, z = sph2cart(azimuth, elevation, 1.0)
    w = np.cos(elevation)

    return x, y, z, w


def surf_plot(x, y, z, c=None):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d', aspect='equal')
    if c is not None:
        c = c - c.min()
        c = c / c.max()
        ax.plot_surface(x, y, z,
                        rstride=1, cstride=1,
                        antialiased=True,
                        facecolors=cm.jet(c),
                        shade=True, linewidth=0.5)
    else:
        ax.plot_surface(x, y, z,
                        rstride=1, cstride=1,
                        antialiased=True,
                        color='y',
                        shade=True, linewidth=0.5)



def lebedev_laikov(order=21):
    pass


def spherical_t_design(n_pts):
    pass
