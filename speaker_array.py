# -*- coding: utf-8 -*-
"""
Created on Sun Jan  4 16:02:15 2015

@author: heller
"""

# python 3.0 compatibility
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy as sp
import scipy.spatial as sps
import scipy.constants as spc

import real_spherical_harmonics as rsh


class Speaker(object):
    """A single speaker"""

    __name_count = 0

    def __init__(self, azimuth, elevation=0.0, radius=1.0,
                 name=None,
                 imaginary=False):
        self.azi = azimuth * Speaker.input_angle_scale
        self.ele = elevation * Speaker.input_angle_scale
        self.rad = radius * Speaker.input_length_scale
        self.imaginary = imaginary  # used by AllRad decoder
        if name:
            self.name = name
        else:
            self.name = "S%02d" % Speaker.__name_count
            Speaker.__name_count += 1

    def __repr__(self):
        return ("<Speaker name: %s, az: %f, el: %f, r: %f>" %
                (self.name, self.azi, self.ele, self.rad))

    def location(self):
        return np.array((self.azi, self.ele, self.rad))

    input_length_scale = 1  # conversion to meters
    input_angle_scale = 1   # conversion to radians

    @classmethod
    def set_length_unit(cls, unit_abbreviation):
        cls.input_length_scale = cls.__unit_dict[unit_abbreviation]

    @classmethod
    def set_angle_unit(cls, unit_abbreviation):
        cls.input_angle_scale = cls.__unit_dict[unit_abbreviation]


class SpeakerImaginary(Speaker):

    def __repr__(self):
        return ("<Imaginary Speaker name: %s, az: %f, el: %f, r: %f>" %
                (self.name, self.azi, self.ele, self.rad))


class InputUnits(object):
    def __init__(self, length_unit="meters", angle_unit='radians'):
        self.length_unit = length_unit
        self.angle_unit = angle_unit
        self.length_scale = InputUnits.__unit_dict[length_unit]
        self.angle_scale = InputUnits.__unit_dict[angle_unit]

    def __repr__(self):
        return "<InputUnits: %s %s>" % (self.length_unit, self.angle_unit)

    __unit_dict = {'feet': spc.foot,
                   'inches': spc.inch,
                   'meters': 1,
                   'cm': 1e-2,
                   'mm': 1e-3,
                   'deg': spc.degree,
                   'degrees': spc.degree,
                   'rad': 1,
                   'radians': 1}


class SpeakerList(list):
    def __init__(self, name):
        self.name = name


class SpeakerArray(object):
    """The speaker array geometry"""
    def __init__(self, name, speaker_or_speakers=None):
        self.name = name
        self.speakers = []
        if speaker_or_speakers:
            self.add_speaker(speaker_or_speakers)

    def __str__(self):
        return "Speaker Array %s, with %d speakers" % (self.name,
                                                       len(self.speakers))

    def __repr__(self):
        return "<SpeakerArray name: %s, size: %d>" % (self.name,
                                                      len(self.speakers))

    def __iter__(self):
        return iter(self.speakers)

    def __nonzero__(self):
        return bool(len(self.speakers))

    def add_speaker(self, speaker_or_speakers):
        """add a speaker to the array"""
        self.speakers.extend(ensure_collection(speaker_or_speakers))
        return self

    def add_array(self, speaker_array):
        return self.add_speaker(speaker_array.speakers)

    def __add__(self, other):
        if isinstance(other, SpeakerArray):
            return self.add_array(other)
        elif isinstance(other, Speaker):
            return self.add_speaker(other)
        else:
            raise Exception("Cannot add a %s to a %s!"
                            % (type(other), type(self)))

    def __getitem__(self, index):
        return self.speakers[index]

    def __getslice__(self, a, b):
        return self.speakers[a:b]

    def azimuths(self):
        return np.array([s.azi for s in self.speakers])

    def elevations(self):
        return np.array([s.ele for s in self.speakers])

    def radii(self):
        return np.array([s.rad for s in self.speakers])

    def locations(self):
        return np.array([s.location() for s in self.speakers])

    def locations_cart(self):
        return np.column_stack(sph2cart(self.azimuths(),
                                        self.elevations(),
                                        self.radii()))

    def directions(self):
        """Cartesian unit vectors in speaker direction"""
        return np.column_stack(sph2cart(self.azimuths(),
                                        self.elevations(),
                                        1.0))

    def names(self):
        return [s.name for s in self.speakers]

    def sph_project(self, degree, order):
        return rsh.real_sph_harm(order, degree,
                                 self.azimuths(),
                                 np.pi/2-self.elevations())

    polygon_names = {3: "Triangle", 4: "Square", 5: "Pentagon", 6: "Hexagon",
                     8: "Octagon"}

    def convex_hull(self):
        return sps.ConvexHull(self.directions())

    class rigs:
        @classmethod
        def polygon(cls, n_speakers=4, radius=1, start_azimuth=0, name=None):
            azimuth_list = start_azimuth + np.linspace(0, 2*np.pi, num=n_speakers, endpoint=False)
            return SpeakerArray(name or "%d-gon" % n_speakers,
                                [Speaker(az, radius=radius)
                                 for az in np.nditer(azimuth_list)])

        @classmethod
        def square(cls, radius=1, start_azimuth=45*spc.degree):
            return cls.polygon(4, radius, start_azimuth, name="Square")

        @classmethod
        def octahedron(cls):
            S = SpeakerArray("Octahedron",
                             (Speaker(0, np.pi/2, 1, name="top"),
                              Speaker(0, -np.pi/2, 1, name="bot")))
            S.add_array(cls.square())
            return S


def cart2sph(x, y, z):
    """convert cartesian coordinates to spherical coordinates"""
    xy = x**2 + y**2
    r = np.sqrt(xy + z**2)
    el = np.arctan2(np.sqrt(xy), z)
    az = np.arctan2(y, x)
    return az, el, r


def sph2cart(az, el, r):
    """convert spherical coordinates to cartesian coordinates"""
    return (r*np.cos(az)*np.cos(el),
            r*np.sin(az)*np.cos(el),
            r*np.sin(el))


def ensure_collection(scalar_or_vector):
    """make sure a is a collection"""
    try:
        len(scalar_or_vector)
    except TypeError:
        scalar_or_vector = [scalar_or_vector]
    return scalar_or_vector


def polygon(n_speakers=4, horizontal_radius=1, start_azimuth=0, height=0):
    azimuth_list = start_azimuth + np.linspace(0, 2*np.pi, num=n_speakers, endpoint=False)
    return SpeakerArray("%d-gon" % n_speakers,
                        [Speaker(az,
                                 radius=horizontal_radius,
                                 elevation=np.arctan2(height, horizontal_radius))

                         for az in np.nditer(azimuth_list)])


def octahedron():
    S = SpeakerArray("Octahedron", (Speaker(0, np.pi/2, 1, name="top"),
                                    Speaker(0, -np.pi/2, 1, name="bot")))
    S.add_array(polygon(4))
    return S


# -----
def unit_test():
    H0 = polygon(8, 3, start_azimuth=np.pi/8, height=0)
    H1 = polygon(6, 2, start_azimuth=0, height=1.5)
    T = polygon(1, 0, 0, 3)

    S = H0+H1+T
    print(S)

    print(S.speakers)

    print(S.sph_project(3, 3))

    return S


if __name__ == '__main__':
    unit_test()


# dead code
#      azimuth = ensure_collection(azimuth)
#        if elevation is None:
#            elevation = np.zeros(np.shape(azimuth))
#        if radius is None:
#            radius = np.ones(np.shape(azimuth))
#        if spkr_id is None:
#            spkr_id = ["S%02d"%n for n in range(self.spkr_count,
#                                                self.spkr_count+len(azimuth))]
#        else:
#            spkr_id = ensure_collection(spkr_id)
#
#
#        self.azi, self.ele, self.rad = (
#            np.append(self.azi, azimuth),
#            np.append(self.ele, elevation),
#            np.append(self.rad, radius))
#
#        self.spkr_id.extend(spkr_id)
#        self.spkr_count += len(spkr_id)
