# -*- coding: utf-8 -*-
from __future__ import division, print_function

"""
Created on Sun Aug 16 10:59:07 2015

@author: heller
"""
"""
This file is part of the Ambisonic Decoder Toolbox (ADT).
Copyright (C) 2015  Aaron J. Heller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sympy.printing import ccode, mathml
from sympy.utilities.mathml import c2p

from symbolic_spherical_harmonics import Ylm, x, y, z, theta, phi

from acn_order import acn2lm

f = open("/Users/heller/spherical_harmonics_orthonormal.html", "w")
f.write("""
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN"
  "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
""")


f.write("""
<HEAD>
<script type="text/javascript",
    src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        "HTML-CSS": { scale: 125, linebreaks: { automatic: true } },
        SVG: { linebreaks: { automatic:true } },
        displayAlign: "left" });
</script>
</HEAD>
""")

f.write("""<BODY>\n""")
f.write("""<TABLE cellpadding="5" border="1" align="left">\n""")
f.write("""
<TR><TH>acn</TH><TH>degree</TH><TH>order</TH>
<TH>expression</TH><TH>c code</TH></TR>
""")

for acn in range(36):
    (l, m) = acn2lm(acn)
    Y = Ylm(acn,
            four_pi=False,
            schmidt_semi=False,
            sph=False,
            condon_shortley_phase=False)
    f.write("""<TR>\n""")
    f.write("""<TD align="center">%i</TD> <TD align="center">%i</TD> <TD align="center">%i</TD>""" % (acn, l ,m))
    f.write("""<TD align="center">%s</TD>\n""" % c2p(mathml(Y)))
    f.write("""<TD align="left"><code>%s</code></TD>\n""" % ccode(Y))
    f.write("""</TR>\n""")

f.write("""</TABLE>\n""")

f.write("""</BODY></HTML>\n""")

f.close()
