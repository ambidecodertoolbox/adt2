# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 10:54:34 2015

@author: heller
"""

from __future__ import division, print_function

import numpy as np
import scipy as sp

#from numpy import sqrt, ones, array, hstack, logical_and, flatnonzero

#                  W | X   Y   Z | R   S   T   U   V | K   L   M   N   O   P   Q
degree = np.array((0,  1,  1,  1,  2,  2,  2,  2,  2,  3,  3,  3,  3,  3,  3,  3))
order =  np.array((0,  1, -1,  0,  0,  1, -1,  2, -2,  0,  1, -1,  2, -2,  3, -3))

# these produce FMS weighting from N3D

norm = np.hstack((1/np.sqrt(2),                                # W
                  1/np.sqrt(3) * np.ones(3),                   # X Y Z
                  1/np.sqrt(5),                                # R
                  1/np.sqrt(5) * 2/np.sqrt(3) * np.ones(4),    # S T U V
                  1/np.sqrt(7),                                # K
                  1/np.sqrt(7) * np.sqrt(45/32) * np.ones(2),  # L M
                  1/np.sqrt(7) * 3/np.sqrt(5) * np.ones(2),    # N O
                  1/np.sqrt(7) * np.sqrt(8/5) * np.ones(2),    # P Q
                  ))

ch_names = 'WXYZRSTUVKLMNOPQ'


def fuma2lm(index):
    return degree[index], order[index]

from collections import Iterable

def index2lm(index):
    try:
        retval = ((degree[i], order[i]) for i in index)
    except TypeError:
        retval = (degree[index], order[index])
    return retval


def fuma(l, m):
    return np.flatnonzero(logical_and(l == degree, m == order))[0]


def fuma_norm(l, m):
    return norm[fuma(l, m)]
