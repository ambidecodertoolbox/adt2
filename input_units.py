# -*- coding: utf-8 -*-
"""
Created on Wed Jan 21 17:56:13 2015

@author: heller
"""


class input_units(object):

    global_coord_code = 'XYZ'
    global_unit_code = 'MMM'

    def __init__(self, coord_code=None, unit_code=None):
        self.coord_code = coord_code
        self.unit_code = unit_code

        self.save_coord_code = input_units.global_coord_code
        self.save_unit_code = input_units.global_unit_code

    def __enter__(self):
        # print "in __enter__", self
        if self.coord_code:
            input_units.global_coord_code = self.coord_code

        if self.unit_code:
            input_units.global_unit_code = self.unit_code

    def __exit__(self, *args):
        if self.coord_code:
            input_units.global_coord_code = self.save_coord_code

        if self.unit_code:
            input_units.global_unit_code = self.save_unit_code


def do_stuff(coord_code=None, unit_code=None):
    coord_code = coord_code or input_units.global_coord_code
    unit_code = unit_code or input_units.global_unit_code
    print coord_code, unit_code

if True:
    do_stuff()

    with input_units("AER", "DDC"):
        do_stuff()
        with input_units(unit_code="abc"):
            do_stuff()
        do_stuff()
        do_stuff(unit_code='ddd')

    do_stuff(unit_code='ccc')


def kwtest(**kws):
    print kws


###
