# -*- coding: utf-8 -*-
"""
Created on Thu Jan 22 15:20:30 2015

@author: heller
"""

from numba import jit
import numpy as np
from real_spherical_harmonics import real_sph_harm


class SphericalDesign(object):

    def __init__(self, size=5200):
        self.spherical = np.loadtxt('/Users/heller/Documents/adt/data/Design_5200_100_random.dat.txt')
        self.uvec = np.array(sph2cart(self.spherical[:, 0],
                                      self.spherical[:, 1],
                                      1.0))

    def az(self):
        return self.spherical[:, 0]

    def el(self):
        return self.spherical[:, 1]

    def w(self):  # dOmega = surface area of unit sphere / number of uniform samples
        return 4*np.pi/len(self.spherical)


def sph2cart(az, el, r):
    """convert spherical coordinates to cartesian coordinates"""
    return (r*np.cos(az)*np.cos(el),
            r*np.sin(az)*np.cos(el),
            r*np.sin(el))


def sph_harm_transform(degree, az, el):
    v = np.zeros(((degree+1)**2, len(az)))
    i = 0
    for l in range(degree+1):
        for m in range(-l, l+1):
            v[i, :] = real_sph_harm(m, l, az, el)
            i += 1
    return v


def acn(l, m):
    return l**2 + l + m
