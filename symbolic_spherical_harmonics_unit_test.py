# -*- coding: utf-8 -*-
from __future__ import division, print_function

"""
Created on Sat Aug 15 16:04:15 2015

@author: heller
"""
"""
This file is part of the Ambisonic Decoder Toolbox (ADT).
Copyright (C) 2015  Aaron J. Heller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sympy import integrate, simplify, lambdify
from sympy import cos, pi
import sympy.mpmath as mpm

# uses in unit tests
from sympy import init_printing, pprint, latex
from contextlib import closing
import multiprocessing as mp
import time

from symbolic_spherical_harmonics import x, y, z, theta, phi
from symbolic_spherical_harmonics import Ylm, Ylm_acn
from acn_order import acn, acn2lm

# confirm that the generated spherical harmonics are orthonormal
#  that means that inner product over the sphere should be 1 if cn1 == cn2,
#  0 otherwise.


def inner_product_over_sphere_symbolic(ex1, ex2):
    "inner product of ex1 and ex2 over the surface of the sphere"
    return integrate(simplify(ex1 * ex2 * cos(phi)),
                     (theta, 0, 2*pi),
                     (phi, -pi/2, pi/2))


def inner_product_over_sphere_numeric(ex1, ex2):
    pi = mpm.pi
    integrand = simplify(ex1 * ex2 * cos(phi))
    fn = lambdify((theta, phi), integrand)
    return mpm.quadgl(fn, (0, 2*pi), (-pi/2, pi/2), maxdegree=1000)


def inner_product_over_sphere(ex1, ex2, symbolic=False):
    if symbolic:
        retval = inner_product_over_sphere_symbolic(ex1, ex2)
    else:
        retval = inner_product_over_sphere_numeric(ex1, ex2)
    return retval


def check_Ylm_ortho_acn(cn1, cn2, verbose=True):

    ex1 = Ylm_acn(cn1, sph=True)
    ex2 = Ylm_acn(cn2, sph=True)

    if verbose:
        print("<", ex1, ",", ex2, ">")

    return check_pair_1arg((cn1, cn2, ex1, ex2),)


def check_pair(fns, i, j):
    (q, abserr) = inner_product_over_sphere(fns[i], fns[j])
    if i == j:
        ok = abs(q-1.0) < max(1e-10, abserr)
    else:
        ok = abs(q) < max(1e-10, abserr)

    if not ok:
        print(i, j, acn2lm(i), acn2lm(j), q, abserr)


#
# use mutiprocessing to take advantage of multiple cores


def check_pair_1arg((i, j, fs1, fs2)):
    start = time.time()
    q = inner_product_over_sphere(fs1, fs2)
    return i, j, q, time.time()-start

import sys
def analyse_pair_1arg((i, j, q, t)):
    if i == j:
        ok = abs(q-1) < 1e-10
    else:
        ok = abs(q) < 1e-10

    if ok:
        print(i, j, acn2lm(i), acn2lm(j), q, t, "OK!!")
    else:
        print(i, j, acn2lm(i), acn2lm(j), q, t, "**** BROKEN!!")

    sys.stdout.flush()


def check_pair_1arg_print((i, j, fs1, fs2)):
    q = inner_product_over_sphere(fs1, fs2)
    analyse_pair_1arg((i, j, q))


def ortho_check_pool(cns=range(acn(10, 10)+1), all=False):
    # generate all the expressions we will use
    print("Generating functions...")
    fns = [simplify(Ylm_acn(i, sph=True))
           if i in cns else None
           for i in range(max(cns)+1)]

    # set up the pool, one process per cpu
    print("Integrating...")
    with closing(mp.Pool(mp.cpu_count())) as pool:
        # dispatch workers
        if all:
            results = [pool.apply_async(check_pair_1arg,
                                        args=((i, j, fns[i], fns[j]),),
                                        callback=analyse_pair_1arg)
                       for i in cns for j in range(i+1)]
        else:
            results = [pool.apply_async(func=check_pair_1arg,
                                        args=((i, i, fns[i], fns[i]),),
                                        callback=analyse_pair_1arg)
                       for i in cns]

    # wait for results to arrive, analyse and summarize
    if False:
        n_results = len(results)
        time.sleep(1)
        while results:
            print("# results:", len(results), '/', n_results)
            for result in results:
                if result.ready():
                    analyse_pair_1arg(result.get())
                    results.remove(result)
            time.sleep(5)
    else:
        for r in results:
            r.wait()


def unit_tests(use_latex=None):
    """
    """
    init_printing(use_latex)
    for l in range(0, 5):
        for m in range(-l, l+1):
            print("% 3d % 3d " % (l, m))
            pprint(Ylm(l, m))


if __name__ == '__main__':
    # unit_tests()
    # check_all_pool(range(100))
    ortho_check_pool(range(9) + range(49, 150) + range(500,510))
