#!/usr/bin/env python  # -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function

"""
Created on Fri Mar 13 17:27:44 2015

@author: heller
"""
# =============================================================================
# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2015  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================

from sympy import re, im, I, Rational, Integer
from sympy import symbols
from sympy import diff, prod, factorial
from sympy import sqrt, pi, cos, sin

from sympy import simplify, nsimplify, expand, lambdify

import sympy.printing as sympr

from acn_order import acn, acn2lm
from furse_malham_order import fuma2lm

import maximize_Ylm as my


#
# Fully normalized (N3D) real spherical harmonics in Cartesian coordinates
#  removed +/- in x +/- yi term because real SH always have positive azimuthal
#  frequency (m) and  + and - selects between cos and sin components
#  See eqns 4-6 in [1] for derivation.
#
#  We assume [x,y,z]^T is a unit vector
#
#  [1]    H. B. Schlegel and M. J. Frisch, “Transformation Between Cartesian
#         and Pure Spherical Harmonic Gaussians,” International Journal of
#         Quantum Chemistry, vol. 54, pp. 83–87, 1995.


x, y, z = symbols('x y z', real=True)
theta, phi, rho = symbols('theta phi rho', real=True)


def factorial_quotient(num, dem):
    "returns num!/dem!, but computed to prevent overflow"

    if num < 0 or dem < 0:
        raise ValueError("num and dem should be non-negative")

    if num > dem:
        return Rational(prod(range(dem+1, num+1)), 1)
    elif num < dem:
        return Rational(1, prod(range(num+1, dem+1)))
    else:
        return Integer(1)

##########################################
# conventions for spherical coordinates
# physics
#   phi is counter-clockwise horizontal angle around the z-axis, 0 is x-axis
#   theta is zenith angle, angle from positive z axis
spherical_coordinates_physics = {
    x: cos(phi) * sin(theta),
    y: sin(phi) * sin(theta),
    z: cos(theta)}

# mathematics
#   theta is counter-clockwise horizontal angle around the z-axis, 0 = x-axis
#   phi is zenith angle, angle from positive z-axis
spherical_coordinates_mathematics = {
    x: cos(theta) * sin(phi),
    y: sin(theta) * sin(phi),
    z: cos(phi)}

# geodesy and ambisonics
#   theta is counter-clockwise horizontal angle around the z-axis, 0 = x-axis
#   phi is elevation, angle from x-y plane
spherical_coordinates_azimuth_elevation = {
    x: cos(theta) * cos(phi),
    y: sin(theta) * cos(phi),
    z: sin(phi)}

#
##########################################


def cartesian_to_spherical(ex,
                           convention=spherical_coordinates_azimuth_elevation):
    return simplify(ex.subs(convention))


def _check_kwargs(sph=None, four_pi=None, condon_shortley_phase=None,
                  normalization=None):
    pass


def legendre_poly(l, m=0, sph=False, **kwargs):

    _check_kwargs(**kwargs)

    if sph:
        zz = sin(phi)
    else:
        zz = x
    return (Ylm_norm(l, m, four_pi=True, **kwargs) *
            Ylm_z(l, m, z_symb=zz, **kwargs))


norm_dict = {
    'none': 0,   # none
    'sch': 1,    # schmidt seminormalized
    'norm': 2,   # fully orthonormalized
    'maxN': 3,   # max = 1 normalized
    'fumaN': 4   # Furse-Malham normalized
    }

norm_dict['unnormalized'] = norm_dict['none']    
norm_dict['semi'] = norm_dict['sch']
norm_dict['SN3D'] = norm_dict['sch']
norm_dict['full'] = norm_dict['norm']
norm_dict['ortho'] = norm_dict['norm']
norm_dict['N3D'] = norm_dict['norm']
norm_dict['fuma'] = norm_dict['fumaN']


def Ylm_norm(l, m, normalization='ortho', four_pi=False, **kwargs):
    mag_m = abs(m)
    norm_sqr = Integer(1)
    norm_code = norm_dict[normalization]

    if norm_code >= norm_dict['sch']:
        norm_sqr *= factorial_quotient(l-mag_m, l+mag_m)
        if mag_m > 0:
            norm_sqr *= 2

    if norm_code >= norm_dict['norm']:
        norm_sqr *= 2*l+1

    if norm_code >= norm_dict['maxN']:
        maxS, maxN, exact = my.max_Ylm((l, m))
        norm_sqr /= maxS**2

    # fumaN is maxN, except that (0,0) is sqrt(2)/2
    if norm_code == norm_dict['fumaN'] and l == 0 and mag_m == 0:
        norm_sqr /= 2

    if not four_pi:
        norm_sqr /= 4*pi

    return sqrt(norm_sqr)


def Ylm_xy(l, m, x_symb=x, y_symb=y, condon_shortley_phase=False, **kwargs):
    "The trigonometric part of the real spherical harmonic"

    a = expand((x_symb + I*y_symb)**abs(m))

    if condon_shortley_phase:
        a *= (-1)**abs(m)

    return im(a) if m < 0 else re(a)


def Ylm_z(l, m, z_symb=z, **kwargs):
    "The Legendre polynomial part of the spherical harmonic"
    return (diff(expand((z_symb**2-1)**l), z_symb, l+abs(m)) /
            (2**l * factorial(l)))


def Ylm_cartesian(l, m, **kwargs):
    "Real spherical harmonic as a polynomial in x, y, z; fast API"
    # print(l, m)
    # print(type(l), type(m))
    return (Ylm_norm(l, m, **kwargs) *
            Ylm_xy(l, m, x, y, **kwargs) *
            Ylm_z(l, m, z, **kwargs))


def Ylm(lm_or_sid, sph=False, sid_fn=acn2lm, **kwargs):
    "Real spherical harmonics; full-featured API"

    _check_kwargs(**kwargs)

    try:
        l, m = lm_or_sid
    except TypeError:
        l, m = sid_fn(lm_or_sid)

    # This workaround is needed because we might get another type of integer,
    # such as numpy.int64, which some versions of SymPy do not recognize see as
    #  an integer.  However, just coercing to a Python int would allow
    # non-integer values of l and m, which would return misleading answers.
    ll = int(l)
    mm = int(m)
    if l == ll and m == mm:
        l = ll
        m = mm
    else:
        raise ValueError("l and m should be integers")

    if l < 0:
        raise ValueError("l should be non-negative")

    if abs(m) > l:
        raise ValueError("|m| should be <= l")

    Y = Ylm_cartesian(l, m, **kwargs)

    if sph:
        Y = cartesian_to_spherical(Y)

    return nsimplify(Y, rational=True, full=True)


def Ylm_acn(channel_number, **kwargs):
    return Ylm(acn2lm(channel_number), **kwargs)


def Ylm_functions(channel_numbers=range(acn(0, 0), acn(3, 3)+1), **kwargs):

    func_args = (theta, phi) if kwargs.get("sph", False) else (x, y, z)

    return [lambdify(func_args, Ylm(cn, **kwargs), modules='numpy')
            for cn in channel_numbers]


def Ylm_function(lm, **kwargs):
    func_args = (theta, phi) if kwargs.get("sph", False) else (x, y, z)

    return lambdify(func_args, Ylm(lm, **kwargs), modules='numpy')

# --------------------
if __name__ == '__main__':
    import argparse
    import sys
    import faustcode

    parser = argparse.ArgumentParser(
        description="returns a polynomial experession for the real spherical" +
                    " harmonic of the specified degree (l) and order (m)")

    parser.add_argument("degree", type=int,
                        help="l, degree of the spherical harmonic. l>=0")
    parser.add_argument("order", type=int, default=0, nargs="?",
                        help="m, order of the spherical harmonic. |m|<=l")

    parser.add_argument("-v", "--verbose", action="store_true",
                        help="increase output verbosity")
    parser.add_argument("-s", "--spherical", action="store_true",
                        help="use spherical coordinates.")
    parser.add_argument("-c", "--cartesian", action="store_true",
                        help="use Cartesian coordinates. (default)")

    unit_group = parser.add_mutually_exclusive_group()
    unit_group.add_argument("--four_pi", action="store_true",
                            help="inner product is 4pi")
    unit_group.add_argument("--unity", action="store_true",
                            help="inner product is 1")

    norm_group = parser.add_mutually_exclusive_group()
    norm_group.add_argument("--orthonormal",
                            action="store_const", const='ortho',
                            help="fully orthonormalized")
    norm_group.add_argument("--seminormalized",
                            action="store_const", const='semi',
                            help="Schmidt seminormalized")
    norm_group.add_argument("--unnormalized",
                            action="store_const", const='unnormalized',
                            help="no normalization")
    norm_group.add_argument("--maxN", action="store_const", const='maxN',
                            help="max gain is 1 (note: may take a while to" +
                                 " compute and result may not exact for l>3")
    norm_group.add_argument("--fumaN", action="store_const", const='fumaN',
                            help="Furse-Malham normalization")

    order_group = parser.add_mutually_exclusive_group()
    order_group.add_argument("--acn", action="store_true",
                             help="interpret degree as ACN-index, m ignored")
    order_group.add_argument("--fuma", action="store_true",
                             help="interpret degree as FuMa-index, m ignored")
    order_group.add_argument("--sid", action="store_true",
                             help="interpret degree as SID-index, m ignored")

    parser.add_argument("--condon_shortley_phase", action="store_true")

    translation_dict = {'fortran': sympr.fcode,
                        'c': sympr.ccode,
                        'javascript': sympr.jscode,
                        'mathml': sympr.mathml,
                        'pretty': sympr.pretty_print,
                        'faust': faustcode.faust_code,
                        'none': lambda x: x}

    parser.add_argument("-t", "--translation",
                        choices=translation_dict.keys(),
                        action="store",
                        default='none')

    # this is a workaround to allow optional arguments that are
    #  negative numbers.  See this post on for more discussion
    #  http://stackoverflow.com/questions/9025204/python-argparse-issue-with-\
    #    optional-arguments-which-are-negative-numbers
    for i, arg in enumerate(sys.argv):
        if (arg[0] == '-') and arg[1].isdigit():
            sys.argv[i] = ' ' + arg

    args = parser.parse_args()

    # default to identity function
    print_fn = translation_dict.get(args.translation, lambda x: x)

    if args.acn:
        lm = acn2lm(args.degree)
    elif args.fuma:
        lm = fuma2lm(args.degree)
    else:
        lm = (args.degree, args.order)

    if args.verbose:
        print("# ", lm)
        print("# ", args)

    print(print_fn(Ylm(lm,
                       sph=args.spherical,
                       normalization=(args.orthonormal or
                                      args.seminormalized or
                                      args.unnormalized or
                                      args.maxN or
                                      args.fumaN or
                                      'ortho'),
                       four_pi=args.four_pi,
                       condon_shortley_phase=args.condon_shortley_phase)))
