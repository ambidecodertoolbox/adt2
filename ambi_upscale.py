# -*- coding: utf-8 -*-
"""
Created on Thu May 28 15:36:57 2015

@author: heller
"""

from __future__ import division, print_function

import numpy as np
import scipy as sp

from contextlib import closing

import matplotlib.pyplot as plt

from scikits import audiolab as al

from librosa import stft, istft, display, logamplitude


def upscale(s, Fs=48000):

    def energy(s):
        return np.real(s * np.conj(s))

    def spec_plot(s, sr=Fs):
        display.specshow(logamplitude(s, ref_power=np.max),
                         sr=Fs,
                         y_axis='log', x_axis='time')
        plt.title('Power Spectrogram')
        plt.colorbar(format='%+2.0f dB')
        plt.tight_layout()
        plt.show()

    dtype = np.complex64
    Dp = np.sqrt(2)*stft(s[:, 0], dtype=dtype)
    Dx = stft(s[:, 1], dtype=dtype)
    Dy = stft(s[:, 2], dtype=dtype)
    Dz = stft(s[:, 3], dtype=dtype)

    E = (energy(Dp) + energy(Dx) + energy(Dy) + energy(Dz)) / 2

    spec_plot(E, Fs)

    Ix = np.real(Dp * Dx)
    Iy = np.real(Dp * Dy)
    Iz = np.real(Dp * Dz)

    #Imag, Idir =

    # coherence between the sound pressure and the acoustical particle velocity
    # "diffuseness" is 1 - coherence

    C2 = sp.sqrt(Ix**2 + Iy**2 + Iz**2) / (E + sp.spacing(1))
    D = 1 - C2
    display.specshow(C2, sr=Fs, y_axis='log', x_axis='time')
    plt.title('Pressure/Velocity Coherence')
    plt.colorbar(format='%+2.1f')
    plt.tight_layout()
    plt.show()


    F_mask = D > 1.1

    Dp[F_mask] = 0.0
    Dx[F_mask] = 0.0
    Dy[F_mask] = 0.0
    Dz[F_mask] = 0.0

    s_U = np.vstack((np.sqrt(0.5) * istft(Dp), istft(Dx), istft(Dy), istft(Dz)))
    al.wavwrite(s_U.T, '/tmp/PP.wav', fs=Fs)

#    DD = D
#    DD[D > 0.01] = 0
#    plt.figure()
#    plt.imshow(DD, origin='lower', aspect='auto', interpolation='nearest')
#    plt.colorbar()
#    plt.show()







def read_wavfile(filename):
    with closing(al.Sndfile(filename, 'r')) as s:
        Fs, nc, enc, fmt = (s.samplerate, s.channels, s.encoding, s.format)
        print(Fs, nc, enc, fmt)
        #  there's a bug in audiolab reading directly in float32
        B = np.float32(s.read_frames(s.nframes, dtype=np.float64))
        print(np.shape(B))
        return Fs, B


if __name__ == '__main__':
    #Fs, S = read_wavfile('/Users/heller/masters/AJH_eight-positions.amb')
    Fs, S = read_wavfile('/Users/heller/masters/Dvorak_CarnivalOverture_Op92-WXYZ.amb')
    upscale(S, Fs=Fs)
