# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

"""
Created on Sun Sep 13 10:18:35 2015

@author: heller
"""

# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2015  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import warnings

import sympy as sym
import symbolic_spherical_harmonics as ssh


def max_Ylm(lm, verbose=False):
    Y = ssh.Ylm(lm, four_pi=True, sph=True)
    Y_symbs = Y.atoms(sym.Symbol)

    theta = ssh.theta
    phi = ssh.phi

    # find the critical points of Y
    partials = [sym.expand(sym.diff(Y, v), trig=True) for v in Y_symbs]
    if verbose:
        print("partials = ", partials)
        print("Y_symbs = ", Y_symbs)
    try:
        Y_cps = sym.solve(partials, Y_symbs, dict=True)
    except TypeError as e:
        if True:
            print(e)
            try:
                Y_cps = sym.nsolve(partials, Y_symbs, dict=True)
            except TypeError as e2:
                return e2
        else:
            return e
    # fill in missing variables to simplify logic later, value doesn't matter
    # and supply numeric values
    Y_cpsN = []
    for cp in Y_cps:
        d = {}
        for s in Y_symbs:
            if not cp.get(s):
                cp[s] = 0
            try:
                d[s] = sym.N(cp[s])
            except:
                print("Cannot evaluate: ", cp[s])
                return cp[s]
        Y_cpsN.append(d)

    if verbose:
        for cpN, cp in zip(Y_cpsN, Y_cps):
            print(cpN, cp)

    # not using second derivative test now, but keep code for reference
    if False:
        #  determinint of hessian (matrix of second partials) for second
        #   derivative test
        H = sym.hessian(Y, (theta, phi))
        D = sym.simplify(sym.det(H))
        D_max = [D.subs(cp) > 0 and H[0, 0].subs(cp) < 0 for cp in Y_cpsN]
        D_min = [D.subs(cp) > 0 and H[0, 0].subs(cp) > 0 for cp in Y_cpsN]
        D_sad = [D.subs(cp) < 0 for cp in Y_cpsN]
        D_unk = [D.subs(cp) == 0 for cp in Y_cpsN]

        if verbose:
            print("\nH = ", H)
            print("\nD = ", D, "\n")
            print("\nD_max = ", D_max, "\n")
            print("\nD_min = ", D_min, "\n")
            print("\nD_sad = ", D_sad, "\n")
            print("\nD_unk = ", D_unk, "\n")

    vals = []
    valsN = []
    cps = []

    for cp, cpN in zip(Y_cps, Y_cpsN):
        val = Y.subs(cp)
        valN = sym.N(val)

        vals.append(val)
        valsN.append(valN)
        cps.append(cp)

    if verbose:
        print(vals)
        print(valsN)

    if vals:
        max_i, max_v = max(enumerate(valsN), key=(lambda x: x[1]))
        min_i, min_v = min(enumerate(valsN), key=(lambda x: x[1]))

        if abs(max_v) > abs(min_v):
            abs_max = vals[max_i]
            abs_maxN = max_v
        else:
            abs_max = -vals[min_i]
            abs_maxN = -min_v

        # try to return a simplified expression, this may fail when
        # there's no closed form for the roots of the equation, so we
        # return the unsimplified experssion and the numeric value
        try:
            s = abs_max
            # this is needed because sym.simplify() doesn't
            # "denest" radicals (get rid of radicals inside radicals)
            # so ...
            s = sym.simplify(s)
            s = sym.sqrtdenest(s)
            s = sym.simplify(s)
        except sym.PolynomialError:
            warnings.warn("No closed form, returning approximate value")
            abs_max = abs_maxN
            exact = False
        else:
            abs_max = s
            exact = True

        return abs_max, abs_maxN, exact
    else:
        # complete failure, return value at (0,0).  Is there something
        # better to do here?
        val = Y.subs({theta: 0, phi: 0})
        return val, val, False  # caller may be expecting two values


if __name__ == '__main__':

    import acn_order as acn
    import furse_malham_order as fuma

    N = 5
    for i in range((N+1)**2):
        if False:
            l, m = fuma.fuma2lm(i)
            print("\n-------\n", i, (l, m), fuma.ch_names[i])
        else:
            l, m = acn.acn2lm(i)
            print("\n-------\n", i, (l, m))

        print("   Ylm = ", ssh.Ylm((l, m), sph=True, four_pi=True))
        Ymax, YmaxN, exact = max_Ylm((l, m), verbose=False)

        print("   N", (l, m), " = ", Ymax)
        print("   exact =", exact)
