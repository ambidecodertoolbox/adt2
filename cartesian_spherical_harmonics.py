# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function

"""
This file has been superceeded by symbolic_spherical_harmonics.py.
I'm keeping it around because of some of the numerical integration stuff.
"""
"""
Created on Fri Mar 13 17:27:44 2015

@author: heller
"""
"""
This file is part of the Ambisonic Decoder Toolbox (ADT).
Copyright (C) 2013  Aaron J. Heller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sympy import re, im
from sympy import symbols
from sympy import diff
from sympy import sqrt, pi
from sympy import cos, sin

from sympy import factorial
from sympy import simplify, nsimplify, expand, lambdify

from acn_order import acn, acn2lm

# uses in unit tests
from sympy import init_printing, pprint, latex
import numpy as np

from numpy import prod
from fractions import Fraction
import matplotlib as mpl

# used by ortho tests
from scipy.integrate import dblquad


x, y, z = symbols('x y z', real=True)
theta, phi = symbols('theta phi', real=True)

# Fully normalized (N3D) real spherical harmonics in Cartesian coordinates
#  no condon-shortly phase (removed +/- in x +/- yi term)
#  we assume
#    . [x,y,z]^T is a unit vector
#    . theta is longitude
#    . phi is elevation
#
#  [1]    H. B. Schlegel and M. J. Frisch, “Transformation Between Cartesian
#         and Pure Spherical Harmonic Gaussians,” International Journal of
#         Quantum Chemistry, vol. 54, pp. 83–87, 1995.


def factorial_quotient(num, dem):
    "returns num!/dem!, but computed to prevent overflow"
    if num > dem:
        return prod(range(dem+1, num+1))
    elif num < dem:
        return Fraction(1, prod(range(num+1, dem+1)))
    else:
        return 1


def Ylm_cart_xy(l, m, x_symb=x, y_symb=y, normalize=True):
    "The trigonometric part of the real spherical harmonic"
    a = nsimplify(expand((x_symb + complex(0, 1)*y_symb)**abs(m)),
                  rational=True, full=True)

    a *= sqrt((2*l+1)*factorial_quotient(l-abs(m), l+abs(m)))
    if normalize:
        a /= sqrt(4*pi)

    if m < 0:
        retval = sqrt(2) * im(a)
    elif m > 0:
        retval = sqrt(2) * re(a)
    else:
        retval = re(a)

    return retval


def Ylm_cart_z(l, m, z_symb=z):
    "The Legendre polynomial part of the spherical harmonic"
    return (diff(expand((z_symb**2-1)**l), z_symb, l+abs(m)) /
            (2**l * factorial(l)))


def Ylm_cart(l, m, x_symb=x, y_symb=y, z_symb=z):
    "Real spherical harmonic as a polynomial in x, y, and z"
    return Ylm_cart_xy(l, m, x_symb, y_symb) * Ylm_cart_z(l, m, z_symb)


def Ylm_cart_acn(channel_number, sph=False):
    if sph:
        xx = cos(theta) * sin(phi)
        yy = sin(theta) * sin(phi)
        zz = cos(phi)
    else:
        xx = x
        yy = y
        zz = z

    l, m = acn2lm(channel_number)
    return Ylm_cart(l, m, x_symb=xx, y_symb=yy, z_symb=zz)


def Ylm_cart_functions(channel_numbers=range(acn(0, 0), acn(3, 3)+1)):
    return [lambdify((x, y, z), Ylm_cart_acn(cn), modules='numpy')
            for cn in channel_numbers]


# confirm that the generated spherical harmonics are orthonormal
#  that means that inner product over the sphere should be 1 if cn1 == cn2,
#  0 otherwise.

def inner_product_over_sphere(f1, f2):
    "inner product of f1 and f2 over the surface of the sphere"

    # arguments to integrand are reversed from args to dblquad (wtf?)
    def integrand(phi, theta):
        # unit vector in direction theta, phi
        ux = np.cos(theta)*np.sin(phi)
        uy = np.sin(theta)*np.sin(phi)
        uz = np.cos(phi)
        # unit area
        d_Omega = np.sin(phi)
        # print((theta, phi, x, y, z))
        # print(f1(x, y, z) * f2(x, y, z) * d_Omega)
        return f1(ux, uy, uz) * f2(ux, uy, uz) * d_Omega

    return dblquad(integrand,
                   0, 2*np.pi,  # range of theta
                   lambda theta: 0, lambda theta: np.pi,  # range of phi
                   epsabs=1.49e-8,  # default
                   epsrel=1.49e-8   # default
                   )


def check_Ylm_cart_ortho_acn(cn1, cn2, verbose=True):

    f1 = lambdify((x, y, z), Ylm_cart_acn(cn1), modules='numpy')
    f2 = lambdify((x, y, z), Ylm_cart_acn(cn2), modules='numpy')

    if verbose:
        print(Ylm_cart_acn(cn1), Ylm_cart_acn(cn2))

    return inner_product_over_sphere(f1, f2)


def check_pair(fns, i, j):
    (q, abserr) = inner_product_over_sphere(fns[i], fns[j])
    if i == j:
        ok = abs(q-1.0) < max(1e-10, abserr)
    else:
        ok = abs(q) < max(1e-10, abserr)

    if not ok:
        print(i, j, acn2lm(i), acn2lm(j), q, abserr)


def check_same(cns=range(acn(15, 15)+1)):
    fns = [lambdify((x, y, z), Ylm_cart_acn(cn), modules='numpy')
           for cn in cns]
    for i in cns:
        check_pair(fns, i, i)

# use mutiprocessing to take advantage of multiple cores
from multiprocessing import Process
def check_all(cns=range(acn(15, 15)+1)):
    fns = [lambdify((x, y, z), Ylm_cart_acn(cn), modules='numpy')
           for cn in cns]

    for i in cns:
        for j in cns:
            p = Process(target=check_pair, args=(fns, i, j))
            p.start()
            #p.join()


from multiprocessing import Pool, cpu_count


def check_pair_1arg((i, j, fs1, fs2)):
    f1 = lambdify((x, y, z), fs1, modules='numpy')
    f2 = lambdify((x, y, z), fs2, modules='numpy')
    (q, abserr) = inner_product_over_sphere(f1, f2)
    return i, j, q, abserr


def check_all_pool(cns=range(acn(10, 10)+1)):
    # multiprocessing pool cannot deal with functions in the lexical closure
    # fns = [lambdify((x, y, z), Ylm_cart_acn(i), modules='numpy') for i in cns]
    fns = [Ylm_cart_acn(i) for i in cns]
    args = [(i, j, fns[i], fns[j]) for i in cns for j in range(i+1)]
    pool = Pool(cpu_count())
    result = pool.map(check_pair_1arg, args)
    pool.close()

    for (i, j, q, abserr) in result:
        if i == j:
            ok = abs(q-1.0) < max(1e-10, abserr)
        else:
            ok = abs(q) < max(1e-10, abserr)
        if not ok:
            print(i, j, acn2lm(i), acn2lm(j), q, abserr)
        else:
            print(i, j, acn2lm(i), acn2lm(j), q, abserr, "OK!")


def lambdify_test(lambdify_modules='numpy'):
    f11_symb = Ylm_cart(1, 1)
    print(f11_symb)
    f11 = lambdify((x, y, z), f11_symb, modules=lambdify_modules)

    f1_1 = lambdify((x, y, z), Ylm_cart(1, -1), modules=lambdify_modules)

    th = np.linspace(0, np.pi*2)
    th_deg = th * 180/np.pi
    mpl.pylab.plot(th_deg, f11(np.cos(th), np.sin(th), 0))
    mpl.pylab.plot(th_deg, f1_1(np.cos(th), np.sin(th), 0))


def unit_tests(use_latex=None):
    """
    """
    init_printing(use_latex)
    for l in range(0, 5):
        for m in range(-l, l+1):
            print("% 3d % 3d " % (l, m))
            pprint(Ylm_cart(l, m))


if __name__ == '__main__':
    unit_tests()
